<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 3/02/18
 * Time: 22:38
 */

namespace Controllers;


use Exceptions\InvalidData;
use Illuminate\Database\Query\Builder;
use Models\User;
use MongoDB\Driver\Query;

class UserController
{

    public function create( $data=[] ){
        try{

            $nombre= $data['name'] ?? '';
            $lastname= $data['lastname'] ?? '';
            $email= $data['email'] ?? '';
            $country= $data['country'] ?? '';
            $password= $data['password'] ?? '';

            $data['password']= md5($data['password']);


            $result=[];
            try{
                $user = User::create($data);
                $user->save();
                $result=[
                    'error' => false,
                    'data'  => [ $user->toArray()],
                    'msg'   => 'OK',
                    'total' => 1
                ];
            }catch (InvalidData $ex){
                $result=[
                    'error' => true,
                    'data'  => [ $ex->getErrors()],
                    'msg'   => 'Error al guardar Usuario',
                    'total' => 1
                ];

            }catch (\Exception $ex){
                $result=[
                    'error' => true,
                    'data'  => [ ],
                    'msg'   => 'Error al guardar Usuario',
                    'total' => 1
                ];
            }



            return json_encode($result);
        }catch (\Exception $e){
            dd($e->getMessage());
        }



    }

    private function validatePassword($pass){
        if(preg_match("/^.*(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $pass) === 0)
            return false;
        return true;
    }

    private function validateEmail($email){
        if(preg_match("/^[a-zA-Z]w+(.w+)*@w+(.[0-9a-zA-Z]+)*.[a-zA-Z]{2,4}$/", $email) === 0)
            return false;
        return true;
    }

    /**
     * @param array $filters
     * @param int $start
     * @param int $perpage
     * @param int $draw
     * @return string
     */
    public function index($filters,$start=0,$perpage=15, $draw=1)
    {

        $query= User::query();
        $total = $query->count();
        if(!is_null($filters) && ! empty($filters)){
            $this->applyFilters($query,$filters);
        }
        //$query->toSql();


        $query = $query->orderBy('id', 'ASC');
        $pag = $query->simplePaginate((int)$perpage, ['*'], 'page')->toArray();
        $result= [
            "draw"=> $draw,
            "recordsTotal"=> $total,
            "recordsFiltered"=> $total,
            "data"=> $pag["data"]
        ];
        return json_encode($result,true);
    }

    /**
     * @param Builder $query
     * @param array $filters
     */
    private function applyFilters($query, $filters){
        foreach ( $filters as $key=>$value){
            if( $key =="and" || $key=="or"){
                $and= $key =="and";
                $query->where(function ( $q ) use ($value,$and){

                    foreach ($value as $filter){
                        $col=  $filter['name'];
                        $op=   $filter['op'];
                        $val=  $filter['value'];
                        if($and===true){
                            $q->where($col,$op,$val);
                        }else{
                            $q->orWhere($col,$op,$val);
                        }

                    }
                });

            }
        }

    }

}