<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 3/02/18
 * Time: 22:38
 */

namespace Controllers;


use Models\User;

class LoginController
{
    public function login($email,$password)
    {
        $pass= md5($password);
        $user = User::where('email',$email)
                     ->where('password',$pass)
                     ->first();
        $result=[
            'error' => false,
            'data'  => [],
            'msg'   => 'OK',
            'total' => 0
        ];
        if( $user != null ){
            $this->startSession($user);
            $result['data']['name'] = $user->name;
            $result['data']['email'] = $user->email;
        }
        else{
            $result['msg']='login failed';
        }
        return json_encode($result);
    }

    private function startSession(User $user){
        @session_destroy();
        session_start();
        $_SESSION['id']=$user->id;
        $_SESSION['name']=$user->name;
        $_SESSION['email']=$user->email;
    }

    public function logout(){
        @session_destroy();
        $result=[
            'error' => false,
            'data'  => [],
            'msg'   => 'OK',
            'total' => 0
        ];
        return json_encode($result);
    }

}