<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 4/02/18
 * Time: 7:48
 */

namespace Controllers;

// the required libs
use Illuminate\View\FileViewFinder;
use Illuminate\Filesystem\Filesystem as Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container as Container;
use Illuminate\View\Factory;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\View as View;


class ViewController
{
    private $baseViews='';

    /**
     * ViewController constructor.
     */
    public function __construct()
    {
        $this->baseViews=dirname(__DIR__).'/views';
    }


    public function testme() {
        $view = $this->makeView('login',[]);
        // create a template
        $r = $this->loadBlade('test',
            $this->baseViews.'/test.blade.php',
                                    array('testvar' => ' timestamp: '.time()) );
        // render the template
        echo $r->render();
    }

    public function login() {
        // create a template
        $view = $this->makeView('login',[]);
        // render the template
        echo $view->render();
    }



    public function registerUser(){

    }

    public function showUsers(){
        // create a template
        $view = $this->makeView('registeruser',[]);
        // render the template
        echo $view->render();
    }

    public function makeView($view,$data){
        $apath = explode('.', $view);
        $path ='';
        foreach ( $apath as $item ){
            $path .= "/$item";
        }
        $path= $path.".blade.php";
        $viewer = $this->loadBlade(
                        $view,
                $this->baseViews.$path,
                        $data
                    );
        return $viewer;
    }

    function loadBlade($view, $viewPath = false, $data = array() ) {

        // echo $this->viewPath;
        if(isset($viewPath)) {
            $viewPath = $viewPath;
        }

        // this path needs to be array
        $FileViewFinder = new FileViewFinder(
            new Filesystem(),
            array($viewPath,dirname(__DIR__).'/views')
        );

        // use blade instead of phpengine
        // pass in filesystem object and cache path
        $compiler = new BladeCompiler(new Filesystem(), dirname(__DIR__,2).'/storage/views/');
        $BladeEngine = new CompilerEngine($compiler);

        $resolver = new EngineResolver;
        $resolver->register('blade', function () use ($compiler) {
            return new CompilerEngine($compiler);
        });

        // create a dispatcher
        $dispatcher = new Dispatcher(new Container());

        // build the factory
        $factory = new Factory(
            $resolver,
            $FileViewFinder,
            $dispatcher
        );

        // this path needs to be string
        $viewObj = new View(
            $factory,
            $BladeEngine,
            $view,
            $viewPath,
            $data
        );

        return $viewObj;

    }

}


/*
// include the autoloader
require __DIR__.'/../app/vendor/autoload.php';
Illuminate\Support\ClassLoader::register();

// create test class
$n = new testClass();
$n->testme();

*/

