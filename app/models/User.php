<?php

namespace Models;


use Exceptions\InvalidData;
use \Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table ="users";
    protected $attributes=[];
    protected $fillable = ['name','lastname','country','email','password'];
    protected $hidden=['password'];
    protected $perPage=10;


    public function save(array $options = [])
    {
        $errors=[];
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $errors['emai']='invalid email';
        }
        if( strlen($this->name)<3 ){
            $errors['name']='invalid name';
        }
        if( strlen($this->lastname)<3 ){
            $errors['lastname']='invalid lastname';
        }
        if( empty( $this->country) ){
            $errors['name']='invalid country';
        }
        if( count($errors)>0 ){
            throw new InvalidData("No se guardo el modelo",0,null,$errors);
        }

        return parent::save($options);

    }


}