<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 7/02/18
 * Time: 21:55
 */

namespace Exceptions;


use Throwable;

class InvalidData extends \Exception
{
    private $errors=[];
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null,array $erros=[])
    {
        $this->errors=$erros;
        parent::__construct($message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }


}