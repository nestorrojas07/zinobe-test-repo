@extends('master')
@section('container')

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <h1 class="text-center login-title">Sign</h1>
                <div class="account-wall">
                    <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                         alt="">
                    <form id="formlogin" class="form-signin" action="#" >
                        <input name="email" type="text" class="form-control" placeholder="Email" required autofocus>
                        <input name="password" type="password" class="form-control" placeholder="Password" required>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                            Sign in</button>
                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me">
                            Remember me
                        </label>
                        <span class="clearfix"></span>
                    </form>
                </div>
                <a href="#" class="text-center new-account" data-toggle="modal" data-target=".bd-example-modal-sm">Create an account </a>
            </div>
        </div>
        <!- ventana modal para registrar usuario -->

        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                </div>
            </div>
        </div>

        <!-- Small modal -->
        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form id="registerUserlogin" class="form-signin" action="#" >
                        <label for="idregname">name</label>
                        <input id="idregname" name="name" type="text" class="form-control" placeholder="name" required autofocus>
                        <label for="idreglastname">last name</label>
                        <input id="idreglastname" name="lastname" type="text" class="form-control" placeholder="last name" required autofocus>
                        <label for="idregemail">email</label>
                        <input id="idregemail" name="email" type="email" class="form-control" placeholder="Email" required autofocus>
                        <label for="idregcountry">country</label>
                        <input id="idregcountry" name="country" type="text" class="form-control" placeholder="country" required autofocus>
                        <ul class="list-group" id="result"></ul>
                        <label for="idregpassword">password</label>
                        <input id="idregpassword" name="password" type="password" class="form-control" placeholder="Password" required>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                            Register</button>
                        <span class="clearfix"></span>
                    </form>
                </div>
            </div>
        </div>

    </div>
    @endsection()
@section('scripts')
    <script>
        $(document).ready(function(){

            $('#formlogin').submit(function(ev) {
                console.log(ev);
                ev.preventDefault();
                console.log($);
                var datapost ={
                    email    : $("input[name=email]").val(),
                    password : $("input[name=password]").val()
                };

                console.log(datapost);
                $.ajax({
                    contentType: 'application/json',
                    data: JSON.stringify(datapost),
                    dataType: 'json',
                    success: function(data){
                        console.log(data);
                        alert("bienvenido");
                        window.location.reload(true);
                    },
                    error: function(){
                        alert("se genero un error al iniciar session")
                    },
                    processData: false,
                    type: 'POST',
                    url: 'services.php?func=login'
                });
            });


            $('#registerUserlogin').submit(function(ev) {
                console.log(ev);
                ev.preventDefault();
                console.log($);
                var formulario = $( this ).serializeArray();
                var datapost = {};
                $.each(formulario,
                    function(i, v) {
                        datapost[v.name] = v.value;
                    });
                console.log(datapost);
                $.ajax({
                    contentType: 'application/json',
                    data: JSON.stringify(datapost),
                    dataType: 'json',
                    success: function(data){
                        console.log(data);
                        if(data.error){
                            var msje="";
                            var p=data.data[0];
                            for (var prop in p ) {
                                console.log(prop);
                                msje = msje + " "+p[prop]+" |  "
                            }
                            alert("Se produjeron los siguientes errores: "+ msje);
                        }
                        else{
                            alert("Registro exitoso");
                            window.location.reload(true);

                        }

                    },
                    error: function(){
                        alert("se genero un error al iniciar session")
                    },
                    processData: false,
                    type: 'POST',
                    url: 'services.php?func=register'
                });
            });

            /////
            $.ajaxSetup({ cache: false });
            $('#idregcountry').keyup(function(){
                $('#result').html('');
                $('#state').val('');
                var searchField = $('#idregcountry').val();
                var expression = new RegExp(searchField, "i");
                $.getJSON('public/pais.json', function(data) {
                    $.each(data, function(key, value){
                        if (value.Name.search(expression) != -1 )
                        {
                            console.log(value);
                            $('#result').append('<li class="list-group-item link-class">'+value.Name+' </li>');
                        }
                    });
                });
            });
            // el onclick de la lista
            $('#result').on('click', 'li', function() {
                var click_text = $(this).text().split('|');
                $('#idregcountry').val($.trim(click_text[0]));
                $("#result").html('');
            });
        });
    </script>
@endsection