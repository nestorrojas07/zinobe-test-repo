@extends('master')
@section('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection()
@section('container')

    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">

        <a class="navbar-brand" href="#">Registros</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <form class="form-inline my-2 my-lg-0">
                <button id="idsalir" class="btn btn-outline-success my-2 my-sm-0" type="button">Salir</button>
            </form>
        </div>
    </nav>



    <table id="idtableuser" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>email</th>
            <th>country</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>email</th>
            <th>country</th>
        </tr>
        </tfoot>
    </table>
@endsection()
@section('scripts')
    <script type="application/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            console.log("se cargo la pagina");
            $('#idtableuser').DataTable(
                {
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url":"services.php?func=listusers",
                        "dataSrc": "data",
                        "type":'GET',
                        "data":function (obj) {
                            console.log(obj);
                            var perpage= obj.length;
                            var start= obj.start;
                            var draw= obj.draw;

                            var page= parseInt(start/perpage)+1;
                            obj={};
                            obj.perpage=perpage;
                            obj.page= page;
                            obj.start=start;
                            obj.draw=draw;


                            var value = $('.dataTables_filter input').val();
                            value= '%'+value+'%';
                            if(value){
                                var filtro={
                                    "or":[
                                        {name:"name", "op":"like","value":value},
                                        {name:"lastname", "op":"like","value":value},
                                        {name:"email", "op":"like","value":value}
                                    ]
                                };
                                obj.filters=JSON.stringify(filtro);
                            }
                            return obj;
                        }

                    },
                    "columns": [
                        { "data": "name" },
                        { "data": "lastname" },
                        { "data": "email" },
                        { "data": "country" }
                    ]
                }
            );
            $('#idsalir').click(function (ev) {
                ev.preventDefault();
                $.ajax({
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function(data){
                        console.log(data);
                        alert("Hasta la proxima");
                        window.location.reload(true);
                    },
                    error: function(){
                        alert("se genero un error al finalizar session")
                    },
                    processData: false,
                    type: 'GET',
                    url: 'services.php?func=logout'
                });
            });
        });
    </script>
@endsection()
