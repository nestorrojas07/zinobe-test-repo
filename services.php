<?php
require 'start.php';
@session_start();
//unset($_SESSION['id']);
//@session_destroy();



$func=$_GET['func'] ?? '';

switch ($func){
    case 'login':{
        $logincontroller= new \Controllers\LoginController();
        $data= json_decode(file_get_contents('php://input'),true);
        $email    = $data['email'] ?? '';
        $password = $data['password']?? '';
        echo $logincontroller->login($email,$password);
        break;
    }
    case 'listusers':{
        $userController= new \Controllers\UserController();
        //$filter='{"or":[{"name":"name","op":"like","value":"%8%"},{"name":"lastname","op":"like","value":"%8%"},{"name":"country","op":"like","value":"%8%"}]}';
        $start   = $_GET['start'] ?? 0;
        $perpage = $_GET['perpage'] ?? 10;
        $filter  = $_GET['filters'] ?? null;
        $draw  = $_GET['draw'] ?? 1;
        $filters = json_decode($filter,true);

        echo $userController->index($filters,$start,$perpage,$draw);
        break;
    }
    case 'register':{
        $userController= new \Controllers\UserController();
        $data= json_decode(file_get_contents('php://input'),true);

        echo $userController->create($data);
        break;
    }
    case 'logout':{
        $logincontroller= new \Controllers\LoginController();
        echo $logincontroller->logout();
        break;
    }
    default:{
        http_response_code(404);
        $result=[
            'error' => true,
            'data'  => [],
            'msg'   => "not found {$func}",
            'total' => 0
        ];
        echo json_encode($result);
        break;
    }
}
