<?php
require __DIR__.'/vendor/autoload.php';
Illuminate\Support\ClassLoader::register();
@session_start();
$session= $_SESSION['id'] ?? null;

$view = new \Controllers\ViewController();
if( is_null($session)){
    $view->login();
}else{
    $view->showUsers();
}
