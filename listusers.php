<?php
$sessionValid= include 'validateSession.php';
if(  !$sessionValid ){
    header('Location: login.php');
    return;
}

$viewController = new \Controllers\ViewController();
$viewController->showUsers();
