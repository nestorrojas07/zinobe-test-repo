<?php
// include the autoloader
require __DIR__.'/vendor/autoload.php';
Illuminate\Support\ClassLoader::register();

// create test class
$n = new \Controllers\ViewController();
$n->login();