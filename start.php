<?php

require 'config.php';
require 'vendor/autoload.php';
//Initialize the class loader
Illuminate\Support\ClassLoader::register();
use Models\Database;
//Initialize Illuminate Database Connection
new Database();
\Illuminate\Pagination\Paginator::currentPageResolver(function ($pageName = 'page') {
    return $_GET['page'];
});