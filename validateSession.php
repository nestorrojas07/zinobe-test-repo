<?php

require __DIR__.'/vendor/autoload.php';
session_start();
$session= $_SESSION['id'] ?? null;

if( ! is_null($session)){
    $n = new \Controllers\ViewController();
    $n->login();
    return  true;
}

return false;